﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="19008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">Instrument driver for an NI-FPGA target (like PCI-7811) that is used as a multi-channel-scaler (MCS). The bitfile of the FPGA target must be selected in the block diagram of the "initialize.vi".

Three digitial lines are used on Connector1
DIO0: start trigger
DIO1: signal input
DIO2: is active

All digital lines use TTL level and trigger on the rising edge. Analog detector signals must be discriminator using a standalone discriminator like a NIM module. The "is active" output can be used to check, if the MCS has received a trigger and/or is just acquiring data.

In case you would like to switch to another target, please do the following.
- Select the approprate bit file in the "initialize" VI of this driver. 
- Open the typedef "fpga_vi_ref.ctl" and select the appropriate bit file as well.
- Save the "fpga_vi_ref.ctl" and, most importantly, perform an "apply all changes". 
- Save the "initialize" and all other VIs.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI

License Agreement for this software:

Copyright (C)
Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany

Contact: D.Beck@gsi.de 

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the license, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General License for more details (http://www.gnu.org).

Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1, 64291 Darmstadt, Germany
For all questions and ideas contact: H.Brand@gsi.de or D.Beck@gsi.de.
Last update: 20-MAY-2008

</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*M!!!*Q(C=Z:1RDB."%%7`U9Y%W4K%"(!''V:%\CO9)^16H$LU&amp;3LV2ER)NMQ6[AK_AE_!.$SX?Y::;@%A!2,3&gt;LM]\6`&gt;67_KWSW6^F+[54`8GKM.@V`M-G\+JR`V]GQOWPA=N7(^R.`XD]&lt;D_H(/4X`44/087).XEH_'@[\J;HN/`K?+]MB`^\HW8`BX1XOG[W@K^V4\=``]]&gt;\.`QH_&lt;:-7B22,,$$(L,R&gt;H_C*HOC*HOC*(OC"(OC"(OC"\OC/\OC/\OC/&lt;OC'&lt;OC'&lt;OC'8DK[U)5O&gt;+K3*%]3*5'4!-FE5*2]*4Q*4]+4]0"4#5`#E`!E0!E05Z4Q*$Q*4]+4],"-#5`#E`!E0!E0I1J*FIY/4]*$?!5]!5`!%`!%0+25Q"-!"-G#Q%%1-"1YAU(!%`!%0!Q6]!1]!5`!%`$A6M!4]!1]!5`!QZ*3F3AUZYY/$W(E]$A]$I`$Y`!17A[0Q_0Q/$Q/$_HE]$A]$I34U!E/AJR&amp;TA4HB]0D]0#1Q_0Q/$Q/D]/$K_S1F]K=;=Y&gt;(2[$R_!R?!Q?AY=1-HA-(I0(Y$&amp;Y##O$R_!R?!Q?AY&gt;5-HA-(I0(!$'3EFZ'-'/B-=E1$"Y_Z&lt;29W;5I*&amp;:[`7N?$KLK!61^7+I(2P5AK'[Q[M;J&lt;IBKI65,K&amp;I9V2&gt;7@2&amp;61.8%KA&amp;6*_L%^R(LM"&lt;&lt;9VNMA[WR&amp;&lt;9],`X,%U_HEY\(I\KO5^OWWO`XWG[XWGQW7K`87KV77C[8YWXVDHZJC_&amp;?WD(O8H^Y/"S_P@^[XX[[`^C_/LTZ]H$7"`^6?`N^M&lt;O^_&lt;XH:.XF8PI@\E;^5$_ZZKH2$R"1LFE!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">0.4.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="bitfiles" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="NI-FPGA-MCS.lvproj_PCI7811R_target.main.vi.lvbitx" Type="Document" URL="../../../FPGA Bitfiles/NI-FPGA-MCS.lvproj_PCI7811R_target.main.vi.lvbitx"/>
		<Item Name="NI-FPGA-MCS.lvproj_PCI7813R_target.main.vi.lvbitx" Type="Document" URL="../../../FPGA Bitfiles/NI-FPGA-MCS.lvproj_PCI7813R_target.main.vi.lvbitx"/>
		<Item Name="NI-FPGA-MCS.lvproj_PCIe7841R_target.main.vi.lvbitx" Type="Document" URL="../../../FPGA Bitfiles/NI-FPGA-MCS.lvproj_PCIe7841R_target.main.vi.lvbitx"/>
		<Item Name="NI-FPGA-MCS.lvproj_PXI7841R_target.main.vi.lvbitx" Type="Document" URL="../../../FPGA Bitfiles/NI-FPGA-MCS.lvproj_PXI7841R_target.main.vi.lvbitx"/>
		<Item Name="NI-FPGA-MCS.lvproj_USB7856R_target.main.vi.lvbitx" Type="Document" URL="../../../FPGA Bitfiles/NI-FPGA-MCS.lvproj_USB7856R_target.main.vi.lvbitx"/>
	</Item>
	<Item Name="typedefs" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="fpga_vi_ref.ctl" Type="VI" URL="../fpga_vi_ref.ctl"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="initialization" Type="Folder">
			<Item Name="initialize.vi" Type="VI" URL="../initialize.vi"/>
			<Item Name="close.vi" Type="VI" URL="../close.vi"/>
		</Item>
		<Item Name="configuration" Type="Folder">
			<Item Name="getBinWidth.vi" Type="VI" URL="../getBinWidth.vi"/>
			<Item Name="getNumberOfBins.vi" Type="VI" URL="../getNumberOfBins.vi"/>
			<Item Name="getNumberOfScans.vi" Type="VI" URL="../getNumberOfScans.vi"/>
			<Item Name="getTriggerMode.vi" Type="VI" URL="../getTriggerMode.vi"/>
			<Item Name="setBinWidth.vi" Type="VI" URL="../setBinWidth.vi"/>
			<Item Name="setNumberOfBins.vi" Type="VI" URL="../setNumberOfBins.vi"/>
			<Item Name="setNumberOfScans.vi" Type="VI" URL="../setNumberOfScans.vi"/>
			<Item Name="setTriggerMode.vi" Type="VI" URL="../setTriggerMode.vi"/>
		</Item>
		<Item Name="actionStatus" Type="Folder">
			<Item Name="startAcquisition.vi" Type="VI" URL="../startAcquisition.vi"/>
			<Item Name="stopAcquisition.vi" Type="VI" URL="../stopAcquisition.vi"/>
			<Item Name="getStatus.vi" Type="VI" URL="../getStatus.vi"/>
		</Item>
		<Item Name="data" Type="Folder">
			<Item Name="waitForData.vi" Type="VI" URL="../waitForData.vi"/>
			<Item Name="waitOnIrq.vi" Type="VI" URL="../waitOnIrq.vi"/>
		</Item>
		<Item Name="utility" Type="Folder">
			<Item Name="get library version.vi" Type="VI" URL="../get library version.vi"/>
			<Item Name="revisionQuery.vi" Type="VI" URL="../revisionQuery.vi"/>
			<Item Name="reset.vi" Type="VI" URL="../reset.vi"/>
			<Item Name="errorQuery.vi" Type="VI" URL="../errorQuery.vi"/>
		</Item>
	</Item>
	<Item Name="applicationExample1.vi" Type="VI" URL="../applicationExample1.vi"/>
	<Item Name="applicationExample2.vi" Type="VI" URL="../applicationExample2.vi"/>
	<Item Name="VI-Tree.vi" Type="VI" URL="../VI-Tree.vi"/>
</Library>
